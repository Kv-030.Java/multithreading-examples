package academy.softserve;

import java.util.concurrent.TimeUnit;

/**
 * Interrupt threads -> for example making I/O operations and the user stop the
 * operation via UI ... we have to terminate the thread
 *
 * boolean Thread.isInterrupted() -> check whether is it interrupted boolean
 * interrupted() -> checks + interrupt the thread !!!
 *
 * Terminate a thread -> volatile boolean flags !!!
 *
 * Thread states:
 *
 * 1.) RUNNABLE: if we create a new thread + call start() method The run()
 * method can be called... new MyThread().start();
 *
 * 2.) BLOCKED: if it is waiting for an object's monitor lock - waiting to enter
 * a synchronized block, like synchronized(new Object()) { } - after wait():
 * waiting for the monitor lock to be free
 *
 * 3.) WAITING: when we call wait() on a thread ... it is going to loose the
 * monitor lock and wait for notify() notifyAll()
 *
 * 4.) TERMINATED: when the run() method is over ... We can check it with
 * isAlive() method
 *
 */
public class Ex4Terminate {
    private static volatile boolean stopRequested;

    public static void main (String[] args) throws InterruptedException {
        Thread backgroundThread = new Thread(() -> {
            int i = 0;
            while (!stopRequested) {
                // sleep means current thread will enter timedwaiting status.
                // It will release CPU for other threads to use and context switch will occur.
                // variables will be reload from main memory after context switch
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                // background thread does io.
                // current thread will enter blocked status.
                // Context switch will occur.
                // variables will be reloaded from main memory after context switch.
                System.out.println(i);

                i++;
            }
        });

        backgroundThread.start();
        TimeUnit.SECONDS.sleep(1);
        stopRequested = true;
    }
}