package academy.softserve;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public final class SimpleFuture implements Future<Result> {
        private final CountDownLatch latch = new CountDownLatch(1);
        private Result value;

        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
            return false;
        }

        @Override
        public boolean isCancelled() {
            return false;
        }

        @Override
        public boolean isDone() {
            return latch.getCount() == 0;
        }

        @Override
        public Result get() throws InterruptedException {
            latch.await();
            return value;
        }

        @Override
        public Result get(long timeout, TimeUnit unit) throws InterruptedException, TimeoutException {
            if (latch.await(timeout, unit)) {
                return value;
            } else {
                throw new TimeoutException();
            }
        }

        // calling this more than once doesn't make sense, and won't work properly in this implementation. so: don't.
        void put(Result result) {
            value = result;
            latch.countDown();
        }
    }

class Result {}