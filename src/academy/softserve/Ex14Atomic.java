package academy.softserve;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * An atomic operation is an operation which is performed as a single unit of work without the possibility
 * of interference from other operations.
 */
public class Ex14Atomic {
    private static AtomicInteger race = new AtomicInteger();
    private static final int THREADS_COUNT = 20;

    private static void increase() {
        race.incrementAndGet();
    }

    public static void main(String[] args) {
        Thread[] threads = new Thread[THREADS_COUNT];

        for (int i = 0; i < THREADS_COUNT; ++i) {
            threads[i] = new Thread(() -> {
                for ( int j = 0; j < 10000; j++ ) {
                    increase();
                }
            });
            threads[i].start();
        }

        while (Thread.activeCount() > 2) {
            Thread.yield();
        }

        System.out.println(race.get());
    }
}
