package academy.softserve;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Ex16CopyOnWrite {
    public static void main(String[] args) throws InterruptedException {
        CopyOnWriteArrayList list=new CopyOnWriteArrayList();

        for (int i = 0; i<100; i++) {
            Thread a1 = new Thread(new addElement(list,i));
            a1.start();
        }
        Thread p1 = new Thread(new printElement(list));
        p1.start();
    }
}

class addElement implements Runnable {
    private List list;
    private int element;
    public addElement (List myList, int e)
    {
        list = myList;
        element = e;
    }

    public void run() { list.add(element); }
}

class printElement implements Runnable {
    private List list;
    public printElement (List myList) { list = myList; }

    public void run() {

        Iterator it=list.iterator();
        while(it.hasNext()) System.out.print(it.next()+" ");
    }
}