package academy.softserve;

/**
 * When a thread invokes a synchronized method, it automatically acquires the intrinsic lock for
 * that method's object and releases it when the method returns.
 *
 * The lock release occurs even if the return was caused by an uncaught exception.
 *
 * When a static synchronized method is invoked, it is associated with a class, not an object.
 * In this case, the thread acquires the intrinsic lock for the !!!!Class!!!! object associated with the class.
 * Thus access to class's static fields is controlled by a lock that's !!!distinct!!! from the lock for any instance of the class.
 */
public class Ex5Synchronized {
    private static int count = 0;

    public static synchronized void increment() {
        count++;
    }

    public static void main(String[] args){
        Thread thread1 = new Thread(() -> {
            for(int i = 0; i < 10000; i++) {
                increment();
            }
        });
        thread1.start();

        Thread thread2 = new Thread(() -> {
            for(int i = 0; i < 10000; i++) {
                increment();
            }
        });
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Count is: " + count);
    }
}
