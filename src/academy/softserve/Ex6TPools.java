package academy.softserve;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * 1.) ExecutorService es = Executors.newCachedThreadPool(); - going to return
 * an executorService that can dynamically reuse threads - before starting a job
 * -> it going to check whether there are any threads that finished the
 * job...reuse them - if there are no waiting threads -> it is going to create
 * another one - good for the processor ... effective solution !!!
 *
 * 2.) ExecutorService es = Executors.newFixedThreadPool(N); - maximize the
 * number of threads - if we want to start a job -> if all the threads are busy,
 * we have to wait for one to terminate
 *
 * 3.) ExecutorService es = Executors.newSingleThreadExecutor(); It uses a
 * single thread for the job
 *
 * execute() -> runnable + callable submit() -> runnable
 *
 */

public class Ex6TPools {
    public static void main(String[] args) {

        ExecutorService executor = Executors.newFixedThreadPool(2);

        for (int i = 0; i < 5; i++) {
            executor.submit(new Processor(i));
        }

        executor.shutdown();

        System.out.println("All tasks submitted.");

        try {
            executor.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {
        }

        System.out.println("All tasks completed.");
    }
}

class Processor implements Runnable {

    private int id;

    public Processor(int id) {
        this.id = id;
    }

    public void run() {
        System.out.println("Starting: " + id);

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
        }

        System.out.println("Completed: " + id);
    }
}