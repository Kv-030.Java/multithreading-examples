package academy.softserve;

public class Ex1BasicThreads {

    public static void main(String[] args) {

        Thread thread1 = new Thread(() -> {
                for ( int i = 0; i < 5; i++ ) {
                    System.out.println("Hello: " + i + " Thread: " + Thread.currentThread().getName());
                    try {
                        Thread.sleep(100);
                    }
                    catch (InterruptedException ignored) {}
                }
            });

        Thread thread2 = new Thread(new RunnerRunnable());

        RunnerThread runner1 = new RunnerThread();

        thread1.start();
        thread2.start();
        runner1.start();
    }
}

class RunnerRunnable implements Runnable {

    @Override
    public void run() {
        for ( int i = 0; i < 5; i++ ) {
            System.out.println("Hello: " + i + " Thread: " + Thread.currentThread().getName());

            try {
                Thread.sleep(100);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class RunnerThread extends Thread {

    @Override
    public void run() {
        for ( int i = 0; i < 5; i++ ) {
            System.out.println("Hello: " + i + " Thread: " + Thread.currentThread().getName());
            try {
                Thread.sleep(100);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
