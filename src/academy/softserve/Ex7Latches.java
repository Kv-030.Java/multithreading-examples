package academy.softserve;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * A synchronization aid that allows one or more threads to wait until a set of operations being performed in other threads completes.
 * A CountDownLatch is initialized with a given count.
 * The await methods block until the current count reaches zero due to invocations of the countDown() method,
 * after which all waiting threads are released and any subsequent invocations of await return immediately.
 * This is a one-shot phenomenon -- the count cannot be reset.
 */
public class Ex7Latches {
    public static void main(String[] args) {

        CountDownLatch latch = new CountDownLatch(3);

        ExecutorService executor = Executors.newFixedThreadPool(3);

        for(int i=0; i < 3; i++) {
            executor.submit(new Driver(latch));
        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Completed.");
    }
}

class Driver implements Runnable {
    private CountDownLatch latch;

    public Driver(CountDownLatch latch) {
        this.latch = latch;
    }

    public void run() {
        System.out.println("Started.");

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        latch.countDown();
    }
}